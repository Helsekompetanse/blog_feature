<?php
/**
 * @file
 * blog_feature.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function blog_feature_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create blog content'.
  $permissions['create blog content'] = array(
    'name' => 'create blog content',
    'roles' => array(
      'Batman' => 'Batman',
      'Robin' => 'Robin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any blog content'.
  $permissions['delete any blog content'] = array(
    'name' => 'delete any blog content',
    'roles' => array(
      'Batman' => 'Batman',
      'Robin' => 'Robin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own blog content'.
  $permissions['delete own blog content'] = array(
    'name' => 'delete own blog content',
    'roles' => array(
      'Batman' => 'Batman',
      'Robin' => 'Robin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in blog_type'.
  $permissions['delete terms in blog_type'] = array(
    'name' => 'delete terms in blog_type',
    'roles' => array(
      'Batman' => 'Batman',
      'Robin' => 'Robin',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any blog content'.
  $permissions['edit any blog content'] = array(
    'name' => 'edit any blog content',
    'roles' => array(
      'Batman' => 'Batman',
      'Robin' => 'Robin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own blog content'.
  $permissions['edit own blog content'] = array(
    'name' => 'edit own blog content',
    'roles' => array(
      'Batman' => 'Batman',
      'Robin' => 'Robin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in blog_type'.
  $permissions['edit terms in blog_type'] = array(
    'name' => 'edit terms in blog_type',
    'roles' => array(
      'Batman' => 'Batman',
      'Robin' => 'Robin',
    ),
    'module' => 'taxonomy',
  );

  return $permissions;
}
